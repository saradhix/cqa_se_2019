from sklearn import linear_model
import pickle
import libunivencoder as emb
#import libinfersent_gl840 as emb
#import libelmo as emb
import preprocessor as p


import xml.etree.ElementTree as ET
from sklearn.model_selection import train_test_split


def read_question_labels_from_xml(input_xml_file):
    labels = {}
    X_all = []
    ids = []
    print('parsing...', input_xml_file)

    tree = ET.parse(input_xml_file)
    root = tree.getroot()
    for thread in root:
        question_tag = thread[0]
        body = question_tag.find("RelQBody").text
        if not body: continue
        question_id = question_tag.attrib['RELQ_ID']
        ids.append(question_id)
        X_all.append(body)
    return X_all, ids

def main():
    filename = 'questions_test.xml'
    outfile = 'predict_questions.txt'
    pickle_file ='xgb_model.pickle'
    pickle_file ='univ_rf_model.pickle'
    pickle_file ='svmrbf_model.pickle'
    model = pickle.load(open(pickle_file, "rb" ))
    print("Loaded model", pickle_file)

    (X_raw_test, ids) = read_question_labels_from_xml(filename)

    print("Test instances=", len(X_raw_test), len(ids))
    X_test = emb.get_vectors(X_raw_test)
    y_pred = model.predict(X_test)
    print(y_pred)
    fp=open(outfile, 'w')
    for (tid, pred) in zip(ids, y_pred):
        fp.write(tid+'\t'+str(pred)+'\n')
    fp.close()




if __name__ == "__main__":
    main()
