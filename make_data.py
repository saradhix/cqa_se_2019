import xml.etree.ElementTree as ET
from sklearn.model_selection import train_test_split

QUESTION_LABELS_MAPPING = {'Opinion': 0, 'Factual': 1, 'Socializing': 2}

def read_question_labels_from_xml(input_xml_file):
    labels = {}
    X_all = []
    y_all = []
    print('parsing...', input_xml_file)

    tree = ET.parse(input_xml_file)
    root = tree.getroot()
    for thread in root:
        question_tag = thread[0]
        body = question_tag.find("RelQBody").text
        if not body: continue
        question_id = question_tag.attrib['RELQ_ID']
        question_fact_label = question_tag.attrib['RELQ_FACT_LABEL']
        label = get_label(question_fact_label, QUESTION_LABELS_MAPPING)
        print(question_fact_label, body, label)
        X_all.append(body)
        y_all.append(label)
        if label > -1:
            labels[question_id] = label

    X_raw_train, X_raw_test, y_train, y_test = train_test_split(X_all, y_all, test_size=0.20, random_state=42)
    fp=open('train.csv','w')
    for (t, l) in zip(X_raw_train, y_train):
        fp.write(str(l)+'\t'+t+'\n')
    fp.close()
    fp=open('test.csv','w')
    for (t, l) in zip(X_raw_test, y_test):
        fp.write(str(l)+'\t'+t+'\n')
    fp.close()

    return labels
def get_label(original_label, label_mapping):
    if original_label in label_mapping.keys():
        return label_mapping[original_label]

    return -1

def main():
    filename = 'questions_train.xml'
    read_question_labels_from_xml(filename)




if __name__ == "__main__":
    main()
